import logging
import os

import matplotlib.pyplot as plt
import numpy as np
from keras import backend as K, Model
from keras.datasets import mnist
from keras.layers import Input, Dense, Lambda
from keras.losses import mse as MinimumSquaredError
from keras.utils import plot_model

logging.basicConfig(format='%(asctime)s - %(levelname)s: %(message)s', datefmt='%d/%m/%Y %I:%M:%S %p',
                    level=logging.INFO)


def plot_results(models, data, batch_size=128, latent_dim_variation=(-3, 3), model_name="vae_mnist"):
    # Plots labels and predicted MNIST digits

    encoder, decoder = models
    x_test, y_test = data
    os.makedirs(model_name, exist_ok=True)

    filename = os.path.join(model_name, "vae_mean.png")
    # display a 2D plot of the digit classes in the latent space
    z_mean, _, _ = encoder.predict(x_test, batch_size=batch_size)
    plt.figure(figsize=(12, 10))
    plt.scatter(z_mean[:, 0], z_mean[:, 1], c=y_test)
    plt.colorbar()
    plt.xlabel("z[0]")
    plt.ylabel("z[1]")
    plt.savefig(filename)
    plt.show()

    filename = os.path.join(model_name, "digits_over_latent.png")
    # display a 30x30 2D manifold of digits
    n = 30
    digit_size = 28
    figure = np.zeros((digit_size * n, digit_size * n))
    (latent_dim_min, latent_dim_max) = latent_dim_variation
    # linearly spaced coordinates corresponding to the 2D plot
    # of digit classes in the latent space
    grid_x = np.linspace(latent_dim_min, latent_dim_max, n)
    grid_y = grid_x[::-1]

    input_mult = decoder.layers[0].input_shape[1] // 2
    for idx, yi in enumerate(grid_y):
        for jdx, xi in enumerate(grid_x):
            samples = np.ndarray.flatten(np.asarray([xi, yi] * input_mult))
            z_sample = np.array([samples])
            x_decoded = decoder.predict(z_sample)
            digit = x_decoded[0].reshape(digit_size, digit_size)
            figure[idx * digit_size: (idx + 1) * digit_size, jdx * digit_size: (jdx + 1) * digit_size] = digit

    plt.figure(figsize=(10, 10))
    start_range = digit_size // 2
    end_range = (n - 1) * digit_size + start_range + 1
    pixel_range = np.arange(start_range, end_range, digit_size)
    sample_range_x = np.round(grid_x, 1)
    sample_range_y = np.round(grid_y, 1)
    plt.xticks(pixel_range, sample_range_x)
    plt.yticks(pixel_range, sample_range_y)
    plt.xlabel("z[0]")
    plt.ylabel("z[1]")
    plt.imshow(figure, cmap='Greys_r')
    plt.savefig(filename)
    plt.show()
    return


def sampling(args):
    mean, log_sigma = args
    batch = K.shape(mean)[0]
    dim = K.int_shape(mean)[1]
    # by default, random_normal has mean = 0 and std = 1.0
    epsilon = K.random_normal(shape=(batch, dim))
    return mean + K.exp(0.5 * log_sigma) * epsilon


def build_model(train, test, input_dim=784, hidden_dim=256, code_dim=2, epochs=50, plot_models=False):
    (x_train, y_train) = train
    (x_test, y_test) = test
    batch_size = 128

    # build encoder model
    inputs = Input(shape=(input_dim,), name='encoder_input')
    x = Dense(hidden_dim, activation='relu')(inputs)
    latent_mean = Dense(code_dim, name='latent_mean')(x)
    latent_log_var = Dense(code_dim, name='latent_log_var')(x)
    sampled_similar_points = Lambda(sampling,
                                    output_shape=(code_dim,),
                                    name='sampled_similar_points')([latent_mean, latent_log_var])
    encoder = Model(inputs, [latent_mean, latent_log_var, sampled_similar_points], name='encoder')
    encoder.summary()

    # build decoder model
    latent_inputs = Input(shape=(code_dim,), name='latent_sampling')
    x = Dense(hidden_dim, activation='relu')(latent_inputs)
    outputs = Dense(input_dim, activation='sigmoid')(x)
    decoder = Model(latent_inputs, outputs, name='decoder')
    decoder.summary()

    # VAE model = encoder + decoder
    outputs = decoder(encoder(inputs)[2])
    vae = Model(inputs, outputs, name='vae_mlp')

    reconstruction_loss = MinimumSquaredError(inputs, outputs) * input_dim
    kl_loss = K.sum(1 + latent_log_var - K.square(latent_mean) - K.exp(latent_log_var), axis=-1) * -0.5
    vae.add_loss(K.mean(reconstruction_loss + kl_loss))
    vae.compile(optimizer='rmsprop')
    vae.summary()

    filename = "vae_mnist_{0}_{1}.h5".format(hidden_dim, code_dim)
    if os.path.isfile(filename):
        logging.info("Loading existing weights for hidden dim {0} and code dim {1} from {2}"
                     .format(hidden_dim, code_dim, filename))
        vae.load_weights(filename)
    else:
        vae.fit(x_train, epochs=epochs, batch_size=batch_size, validation_data=(x_test, None))
        logging.info("Saving weights for hidden dim {0} and code dim {1} to {2}".format(hidden_dim, code_dim, filename))
        vae.save_weights(filename)

    if plot_models:
        logging.info("Plotting model diagrams for encoder, decoder, and VAE, and saving to `graphs/`")
        plot_model(encoder, to_file='graphs/vae_mlp_encoder.png', show_shapes=True)
        plot_model(decoder, to_file='graphs/vae_mlp_decoder.png', show_shapes=True)
        plot_model(vae, to_file='graphs/vae_mlp.png', show_shapes=True)
    plot_results(models=(encoder, decoder),
                 data=test,
                 batch_size=batch_size,
                 latent_dim_variation=(-3, 3),
                 model_name="vae_mnist")
    return vae


def main():
    (x_train, y_train), (x_test, y_test) = mnist.load_data()
    input_dimensions = x_train.shape[1] * x_train.shape[2]
    x_train = x_train.reshape(x_train.shape[0], input_dimensions).astype(np.float32) / 255
    x_test = x_test.reshape(x_test.shape[0], input_dimensions).astype(np.float32) / 255
    auto_encoder = build_model(train=(x_train, y_train),
                               test=(x_test, y_test),
                               input_dim=input_dimensions,
                               hidden_dim=256,
                               code_dim=8,
                               # 50 Epochs seems like the sweet spot with 256, 2
                               epochs=50,
                               plot_models=True)
    return


if __name__ == "__main__":
    main()
