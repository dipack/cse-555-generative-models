# Inspiration from:
# https://github.com/mchablani/deep-learning/blob/master/gan_mnist/Intro_to_GANs_Exercises.ipynb

import os
import logging
import pickle as pkl
import numpy as np
import tensorflow as tf
import matplotlib.pyplot as plt
from tensorflow.keras.datasets import mnist

logging.basicConfig(format='%(asctime)s - %(levelname)s: %(message)s', datefmt='%d/%m/%Y %I:%M:%S %p',
                    level=logging.INFO)


def model_inputs(real_dim, latent_dim):
    inputs_real = tf.placeholder(tf.float32, (None, real_dim), name="inputs_real")
    inputs_latent = tf.placeholder(tf.float32, (None, latent_dim), name="inputs_latent")

    return inputs_real, inputs_latent


def generator(z, out_dim, n_units=128, reuse=False, alpha=0.01):
    ''' Build the generator network.

        Arguments
        ---------
        z : Input tensor for the generator
        out_dim : Shape of the generator output
        n_units : Number of units in hidden layer
        reuse : Reuse the variables with tf.variable_scope
        alpha : leak parameter for leaky ReLU

        Returns
        -------
        out, logits:
    '''
    with tf.variable_scope('generator', reuse=reuse):
        # Hidden layer
        h1 = tf.layers.dense(z, n_units, activation=None)
        # Leaky ReLU
        h1 = tf.maximum(h1, alpha * h1)

        # Logits and tanh output
        logits = tf.layers.dense(h1, out_dim, activation=None)
        out = tf.nn.tanh(logits)

        return out, logits


def discriminator(x, n_units=128, reuse=False, alpha=0.01):
    ''' Build the discriminator network.

        Arguments
        ---------
        x : Input tensor for the discriminator
        n_units: Number of units in hidden layer
        reuse : Reuse the variables with tf.variable_scope
        alpha : leak parameter for leaky ReLU

        Returns
        -------
        out, logits:
    '''
    with tf.variable_scope('discriminator', reuse=reuse):
        # Hidden layer
        h1 = tf.layers.dense(x, n_units, activation=None)
        # Leaky ReLU
        h1 = tf.maximum(h1, alpha * h1)

        logits = tf.layers.dense(h1, 1, activation=None)
        out = tf.nn.sigmoid(logits)

        return out, logits


def view_samples(epoch, samples):
    fig, axes = plt.subplots(figsize=(7, 7), nrows=4, ncols=4, sharey=True, sharex=True)
    images = [img.reshape((28, 28)) for img in samples[epoch][0]]
    for idx, ax in enumerate(axes.flatten()):
        ax.xaxis.set_visible(False)
        ax.yaxis.set_visible(False)
        im = ax.imshow(images[idx], cmap='Greys_r')
    plt.show()
    return fig, axes


def construct_graph(input_size=784, gen_hidden_units=128, dis_hidden_units=128, latent_units=100):
    """
    Construct the network
    :param input_size: Size of the input vector
    :param gen_hidden_units: Number of hidden units in the generator
    :param dis_hidden_units: Number of hidden units in the discriminator
    :param latent_units: Size of the latent vector
    :return:
    """
    # Leak factor for leaky ReLU
    alpha = 0.01
    # Label smoothing
    smooth = 0.1
    tf.reset_default_graph()
    # Create our input placeholders
    input_real, input_latent = model_inputs(input_size, latent_units)

    # Generator network here
    gen_model, gen_logits = generator(input_latent, input_size, gen_hidden_units, reuse=False, alpha=alpha)
    # gen_model is the generator output

    # Disriminator network here
    dis_model_real, dis_logits_real = discriminator(input_real, dis_hidden_units, reuse=False, alpha=alpha)
    dis_model_fake, dis_logits_fake = discriminator(gen_model, dis_hidden_units, reuse=True, alpha=alpha)

    # Calculate losses
    dis_labels_real = tf.ones_like(dis_logits_real) * (1 - smooth)
    dis_labels_fake = tf.zeros_like(dis_logits_fake)

    dis_loss_real = tf.nn.sigmoid_cross_entropy_with_logits(labels=dis_labels_real, logits=dis_logits_real)
    dis_loss_fake = tf.nn.sigmoid_cross_entropy_with_logits(labels=dis_labels_fake, logits=dis_logits_fake)

    dis_loss = tf.reduce_mean(dis_loss_real + dis_loss_fake)

    gen_loss = tf.reduce_mean(
        tf.nn.sigmoid_cross_entropy_with_logits(
            labels=tf.ones_like(dis_logits_fake),
            logits=dis_logits_fake))

    # Optimizers
    learning_rate = 0.002

    # Get the trainable_variables, split into G and D parts
    training_vars = tf.trainable_variables()
    gen_vars = [var for var in training_vars if var.name.startswith("generator")]
    dis_vars = [var for var in training_vars if var.name.startswith("discriminator")]

    disc = tf.train.AdamOptimizer().minimize(dis_loss, var_list=dis_vars)
    gen = tf.train.AdamOptimizer().minimize(gen_loss, var_list=gen_vars)

    return (gen, gen_loss, gen_vars), (disc, dis_loss, dis_vars), (input_real, input_latent)


def fight(x,
          input_size,
          gen_hidden_units,
          dis_hidden_units,
          latent_units,
          batch_size,
          epochs,
          generate_samples=True,
          samples_pkl_filename="train_samples.pkl"):
    logging.info("Constructing graph")
    (gen, gen_loss, gen_vars), (disc, dis_loss, dis_vars), (input_real, input_latent) = construct_graph(
        input_size=input_size,
        gen_hidden_units=gen_hidden_units,
        dis_hidden_units=dis_hidden_units,
        latent_units=latent_units)

    samples = []
    losses = []
    saver = tf.train.Saver(var_list=gen_vars)
    logging.info("Generating adversarial images for discriminator")
    with tf.Session() as sess:
        sess.run(tf.global_variables_initializer())
        for e in range(epochs):
            start_idx = 0
            for ii in range(x.shape[0] // batch_size):
                # Get images, reshape and rescale to pass to discriminator
                batch_images = x[start_idx:start_idx + batch_size, :]
                batch_images = batch_images * 2 - 1
                start_idx += batch_size

                # Sample random noise for generator
                batch_z = np.random.uniform(-1, 1, size=(batch_size, latent_units))

                # Run one epoch of each network
                _ = sess.run(disc, feed_dict={input_real: batch_images, input_latent: batch_z})
                _ = sess.run(gen, feed_dict={input_latent: batch_z})

            # At the end of each epoch, get the losses and print them out
            dis_train_loss = sess.run(dis_loss, {input_latent: batch_z, input_real: batch_images})
            gen_train_loss = gen_loss.eval({input_latent: batch_z})

            logging.info("Epoch {}/{}...".format(e + 1, epochs) +
                         "Discriminator Loss: {:.4f}...".format(dis_train_loss) +
                         "Generator Loss: {:.4f}".format(gen_train_loss))
            # Save losses to view after training
            losses.append((dis_train_loss, gen_train_loss))

            # Sample from generator as we're training for viewing afterwards
            latent_sample = np.random.uniform(-1, 1, size=(16, latent_units))
            generated_samples = sess.run(
                generator(input_latent, input_size, gen_hidden_units, reuse=True),
                feed_dict={input_latent: latent_sample})
            samples.append(generated_samples)
            saver.save(sess, './checkpoints/generator.ckpt')

    # Save training generator samples
    if samples_pkl_filename is not None:
        logging.info("Saving samples as a pickle to {0}".format(samples_pkl_filename))
        with open(samples_pkl_filename, 'wb') as f:
            pkl.dump(samples, f)

    if generate_samples:
        logging.info("Generating samples")
        saver = tf.train.Saver(var_list=gen_vars)
        with tf.Session() as sess:
            saver.restore(sess, tf.train.latest_checkpoint('checkpoints'))
            latent_sample = np.random.uniform(-1, 1, size=(16, latent_units))
            generated_samples = sess.run(
                generator(input_latent, input_size, gen_hidden_units, reuse=True),
                feed_dict={input_latent: latent_sample})
        view_samples(0, [generated_samples])
    return samples, (gen, gen_loss, gen_vars), (disc, dis_loss, dis_vars), (input_real, input_latent)


def main():
    (x_train, y_train), (_, _) = mnist.load_data()
    image_dim = x_train.shape[1]
    x_train = np.reshape(x_train, (-1, image_dim * image_dim)).astype(np.float32) / 255

    input_size = image_dim * image_dim
    gen_hidden_units = dis_hidden_units = 256
    z_size = 100
    batch_size = 100
    epochs = 100
    samples_pkl = "train_samples.pkl"

    if os.path.isfile(samples_pkl):
        # Load samples from generator taken while training
        logging.info("Loading existing samples from {0}".format(samples_pkl))
        with open(samples_pkl, 'rb') as f:
            samples = pkl.load(f)
    else:
        samples, *_ = fight(x=x_train,
                            input_size=input_size,
                            gen_hidden_units=gen_hidden_units,
                            dis_hidden_units=dis_hidden_units,
                            latent_units=z_size,
                            batch_size=batch_size,
                            epochs=epochs,
                            samples_pkl_filename=samples_pkl)

    _ = view_samples(-1, samples)

    logging.info("Displaying images generated, every 10 epochs")
    rows, cols = 10, 6
    fig, axes = plt.subplots(figsize=(7, 12), nrows=rows, ncols=cols, sharex=True, sharey=True)

    for sample, ax_row in zip(samples[::int(len(samples) / rows)], axes):
        images = [img.reshape((28, 28)) for img in sample[::int(len(sample[0]) / cols)][0]]
        for idx, ax in enumerate(ax_row):
            ax.xaxis.set_visible(False)
            ax.yaxis.set_visible(False)
            ax.imshow(images[idx], cmap='Greys_r')
    plt.show()
    return


if __name__ == '__main__':
    main()
