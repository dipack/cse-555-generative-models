import logging
import os
import matplotlib.pyplot as plt
from keras import backend as K
from keras.callbacks import TensorBoard, EarlyStopping
from keras.datasets import mnist
from keras.layers import MaxPooling2D, UpSampling2D, Conv2D
from keras.models import Sequential
from keras.utils import to_categorical

logging.basicConfig(format='%(asctime)s - %(levelname)s: %(message)s', datefmt='%d/%m/%Y %I:%M:%S %p',
                    level=logging.INFO)


def format_data(X, y=None, num_classes=10):
    # input image dimensions
    img_rows, img_cols = 28, 28

    if K.image_data_format() == 'channels_first':
        X = X.reshape(X.shape[0], 1, img_rows, img_cols)
        input_shape = (1, img_rows, img_cols)
    else:
        X = X.reshape(X.shape[0], img_rows, img_cols, 1)
        input_shape = (img_rows, img_cols, 1)

    X = X.astype("float32")
    X /= 255

    if y is not None:
        y = to_categorical(y, num_classes)

    return X, y, input_shape


def build_model(input_shape):
    encoder_layers = [
        # Input(shape=input_shape),
        Conv2D(16, (3, 3), activation='relu', padding='same', input_shape=input_shape),
        MaxPooling2D((2, 2), padding='same'),
        Conv2D(8, (3, 3), activation='relu', padding='same'),
        MaxPooling2D((2, 2), padding='same'),
        Conv2D(8, (3, 3), activation='relu', padding='same'),
        MaxPooling2D((2, 2), padding='same')
    ]
    decoder_layers = [
        Conv2D(8, (3, 3), activation='relu', padding='same'),
        UpSampling2D((2, 2)),
        Conv2D(8, (3, 3), activation='relu', padding='same'),
        UpSampling2D((2, 2)),
        Conv2D(16, (3, 3), activation='relu'),
        UpSampling2D((2, 2)),
        Conv2D(1, (3, 3), activation='sigmoid', padding='same'),
    ]
    model = Sequential(layers=[
        *encoder_layers,
        *decoder_layers
    ])
    model.compile(optimizer='rmsprop', loss='binary_crossentropy')
    model.summary()
    return model


def train_model(model, x_train, x_test):
    model_filename = "cae_mnist.h5"
    if os.path.isfile(model_filename):
        logging.info("Loading existing model weights from {0}".format(model_filename))
        model.load_weights(model_filename)
    else:
        tensorboard = TensorBoard(log_dir='/tmp/tensorboard')
        earlystopper = EarlyStopping(patience=1)
        model.fit(x_train, x_train,
                  epochs=50,
                  batch_size=128,
                  shuffle=True,
                  validation_data=(x_test, x_test),
                  callbacks=[tensorboard, earlystopper])
        logging.info("Saving model weights to {0}".format(model_filename))
        model.save_weights(model_filename)
    return model


def main():
    num_classes = 10
    (x_train, _), (x_test, _) = mnist.load_data()

    logging.info("Formatting data")
    x_train, _, input_shape = format_data(x_train, num_classes=num_classes)
    x_test, _, input_shape = format_data(x_test, num_classes=num_classes)

    logging.info("Constructing model")
    cae = build_model(input_shape)

    logging.info("Training model")
    cae = train_model(cae, x_train, x_test)

    logging.info("Generating images using test data")
    decoded_imgs = cae.predict(x_test)

    n = 10
    plt.figure(figsize=(20, 4))
    for i in range(n):
        # display original
        ax = plt.subplot(2, n, i + 1)
        plt.imshow(x_test[i].reshape(28, 28))
        ax.set_title("Original")
        plt.gray()
        ax.xaxis.set_visible(False)
        ax.yaxis.set_visible(False)

        # display reconstruction
        ax = plt.subplot(2, n, i + n + 1)
        plt.imshow(decoded_imgs[i].reshape(28, 28))
        ax.set_title("Generated")
        plt.gray()
        ax.xaxis.set_visible(False)
        ax.yaxis.set_visible(False)
    plt.savefig("generated.png")
    plt.show()
    return


if __name__ == "__main__":
    main()
