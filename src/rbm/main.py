import logging
import numpy as np
import matplotlib.pyplot as plt
import tensorflow as tf
from tensorflow.keras.datasets import mnist
from util import tf_xavier_init, sample_bernoulli, sample_gaussian

logging.basicConfig(format='%(asctime)s - %(levelname)s: %(message)s', datefmt='%d/%m/%Y %I:%M:%S %p',
                    level=logging.INFO)


class RestrictedBoltzmannMachine(object):
    def __init__(self,
                 visible_units,
                 hidden_units,
                 learning_rate=0.01,
                 regularizer=0.95,
                 sample_visible=False,
                 sigma=1,
                 xavier_const=1.0):
        self.sample_visible = sample_visible
        self.sigma = sigma

        self.visible_units = visible_units
        self.hidden_units = hidden_units
        self.learning_rate = learning_rate
        self.regularizer = regularizer

        self.visible_layer = tf.placeholder(tf.float32, [None, self.visible_units])
        self.visible_bias = tf.Variable(tf.zeros([self.visible_units]), dtype=tf.float32)

        self.hidden_layer = tf.placeholder(tf.float32, [None, self.hidden_units])
        self.hidden_bias = tf.Variable(tf.zeros([self.hidden_units]), dtype=tf.float32)

        self.weights = tf.Variable(tf_xavier_init(self.visible_units, self.hidden_units, const=xavier_const),
                                   dtype=tf.float32)

        self.weights_delta = tf.Variable(tf.zeros([self.visible_units, self.hidden_units]), dtype=tf.float32)
        self.visible_bias_delta = tf.Variable(tf.zeros([self.visible_units]), dtype=tf.float32)
        self.hidden_bias_delta = tf.Variable(tf.zeros([self.hidden_units]), dtype=tf.float32)

        self.updated_weights = None
        self.updated_deltas = None
        self.compute_hidden = None
        self.compute_visible = None
        self.compute_visible_from_hidden = None

        self._initialize_vars()

        self.compute_error = tf.reduce_mean(tf.square(self.visible_layer - self.compute_visible))

        init = tf.global_variables_initializer()
        self.sess = tf.Session()
        self.sess.run(init)
        return

    def _initialize_vars(self):
        hidden_layer_activation = tf.nn.sigmoid(tf.matmul(self.visible_layer, self.weights) + self.hidden_bias)

        visible_layer_reconstruction_activation = tf.matmul(sample_bernoulli(hidden_layer_activation),
                                                            tf.transpose(self.weights)) + self.visible_bias

        hidden_layer_reconstruction_activation = tf.nn.sigmoid(
            tf.matmul(visible_layer_reconstruction_activation, self.weights) + self.hidden_bias)

        positive_gradient = tf.matmul(tf.transpose(self.visible_layer), hidden_layer_activation)
        negative_gradient = tf.matmul(tf.transpose(visible_layer_reconstruction_activation),
                                      hidden_layer_reconstruction_activation)

        def weight_update(weights_prev, weights_new):
            return self.regularizer * weights_prev + \
                   self.learning_rate * weights_new * (1 - self.regularizer) / tf.to_float(tf.shape(weights_new)[0])

        weights_delta_new = weight_update(self.weights_delta, positive_gradient - negative_gradient)
        visible_bias_delta_new = weight_update(self.visible_bias_delta,
                                               tf.reduce_mean(
                                                   self.visible_layer - visible_layer_reconstruction_activation, 0))
        hidden_bias_delta_new = weight_update(self.hidden_bias_delta,
                                              tf.reduce_mean(
                                                  hidden_layer_activation - hidden_layer_reconstruction_activation, 0))

        updated_weights_delta = self.weights_delta.assign(weights_delta_new)
        updated_visible_bias_delta = self.visible_bias_delta.assign(visible_bias_delta_new)
        updated_hidden_bias_delta = self.hidden_bias_delta.assign(hidden_bias_delta_new)

        updated_weights = self.weights.assign(self.weights + weights_delta_new)
        updated_visible_bias = self.visible_bias.assign(self.visible_bias + visible_bias_delta_new)
        updated_hidden_bias = self.hidden_bias.assign(self.hidden_bias + hidden_bias_delta_new)

        self.updated_deltas = [updated_weights_delta, updated_visible_bias_delta, updated_hidden_bias_delta]
        self.updated_weights = [updated_weights, updated_visible_bias, updated_hidden_bias]

        self.compute_hidden = tf.nn.sigmoid(tf.matmul(self.visible_layer, self.weights) + self.hidden_bias)
        self.compute_visible = tf.matmul(self.compute_hidden, tf.transpose(self.weights)) + self.visible_bias
        self.compute_visible_from_hidden = tf.matmul(self.hidden_layer, tf.transpose(self.weights)) + self.visible_bias
        return

    def get_error(self, batch_x):
        return self.sess.run(self.compute_error, feed_dict={self.visible_layer: batch_x})

    def predict(self, batch_x):
        return self.sess.run(self.compute_visible, feed_dict={self.visible_layer: batch_x})

    def partial_fit(self, batch_x):
        self.sess.run(self.updated_weights + self.updated_deltas, feed_dict={self.visible_layer: batch_x})

    def fit(self, x, num_epochs=10, batch_size=10):
        rows = x.shape[0]
        if batch_size > 0:
            num_batches = rows // batch_size + (0 if rows % batch_size == 0 else 1)
        else:
            num_batches = 1
        np.random.shuffle(x)
        errors = []

        for epoch in range(num_epochs):
            logging.info('Epoch: {0}'.format(epoch))

            epoch_errors = np.zeros((num_batches,))
            epoch_idx = 0

            for batch in range(num_batches):
                batch_x = x[batch * batch_size:(batch + 1) * batch_size]
                self.partial_fit(batch_x)
                batch_error = self.get_error(batch_x)
                epoch_errors[epoch_idx] = batch_error
                epoch_idx += 1
            mean_error = np.mean(epoch_errors)
            logging.info('Train error: {0}'.format(mean_error))
            errors = np.hstack([errors, epoch_errors])
        return errors

    def save_weights(self, filename, name):
        saver = tf.train.Saver({name + '_w': self.weights,
                                name + '_v': self.visible_bias,
                                name + '_h': self.hidden_bias})
        return saver.save(self.sess, filename)

    def load_weights(self, filename, name):
        saver = tf.train.Saver({name + '_w': self.weights,
                                name + '_v': self.visible_bias,
                                name + '_h': self.hidden_bias})
        saver.restore(self.sess, filename)
        return


def plot_images(x, filename="generated.png", nrows=4, ncols=4):
    random_indexes = np.random.randint(len(x), size=nrows * ncols)
    sample_images = x[random_indexes, :]
    figure, axes = plt.subplots(figsize=(7, 12), nrows=nrows, ncols=ncols)
    counter = 0
    for ax_row in axes:
        for idx, ax in enumerate(ax_row):
            reshaped = np.reshape(sample_images[counter], (28, 28))
            ax.xaxis.set_visible(False)
            ax.yaxis.set_visible(False)
            ax.imshow(reshaped, cmap='Greys_r')
            counter += 1
    logging.info("Saving image to {0}".format(filename))
    plt.savefig(filename)
    plt.show()
    return


def remove_pixels(x, percent=0.2):
    x = np.asarray(x)
    for row in x:
        indices = np.random.choice(np.arange(row.size), replace=False, size=int(row.size * percent))
        row[indices] = 0.0
    return x


def main():
    visible_units = 784
    hidden_units = 20
    epochs = 20
    removal_percent = 0.5

    weights_filename = "./weights/rbm_{0}".format(hidden_units)
    weights_name = "rbm_{0}".format(hidden_units)
    generated_filename = "graphs/{0}_{1}_percent.png".format(weights_name, int(removal_percent * 100))

    (x_train, y_train), (x_test, y_test) = mnist.load_data()

    x_train = x_train.reshape(x_train.shape[0], x_train.shape[1] * x_train.shape[2]).astype(np.float32)
    x_test = x_test.reshape(x_test.shape[0], x_test.shape[1] * x_test.shape[2]).astype(np.float32)
    x_test_hobbled = remove_pixels(x_test, removal_percent)

    rbm = RestrictedBoltzmannMachine(visible_units=visible_units, hidden_units=hidden_units, learning_rate=0.1,
                                     sample_visible=False)
    try:
        rbm.load_weights(weights_filename, weights_name)
    except ValueError:
        logging.warning("Failed to read weights from file, so training from scratch")
        rbm.fit(x_train, num_epochs=epochs)
        rbm.save_weights(weights_filename, weights_name)
    predictions = rbm.predict(x_test_hobbled)
    plot_images(predictions, generated_filename)
    return


if __name__ == "__main__":
    main()
